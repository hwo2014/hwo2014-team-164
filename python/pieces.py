# coding=utf-8
__author__ = 'herkko'
import math

class Piece(object):
    def __init__(self):
        print("data in piece initialization")
        self.bend = False
        self.index = 0
        self.length = 0
        self.switch = False
        self.radius = 0
        self.angle = 0
        self.next = 0


    def initpiece(self, index, data):
        self.index = index
        self.next = (index+1)
        print(self.next)
        if data.has_key("length"):
            self.length = data['length']

        if data.has_key("switch"):
            self.switch = data['switch']

        if data.has_key("radius"):
            self.radius = data['radius']

        if data.has_key("angle"):
            self.angle = data['angle']
        if self.length ==0:
            self.calculatebend()



    def isbend(self):
        if self.radius == 0:
            return False
        else:
            return True

    def nextpiece(self):
        return next

    def calculatebend(self):
        self.length = math.fabs((self.angle/360)*2*3.14*self.radius)



