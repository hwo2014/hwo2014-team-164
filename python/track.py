__author__ = 'herkko'
from pieces import Piece

class Track(object):

    def __init__(self):
        self.name = "default"
        self.pieces = []
        self.piececount = 0
        self.driftstarted = -1
        self.maxdrift = -1
        self.maxspeed = -1
        self.driftstartspeed = 0
        self.dirftendspeed  = 0
        self.driftended = -1
        self.speedvalues = []
        self.anglevalues = []
        self.piecelengths = []



    def initpieces(self, data):
        allpieces = data["race"]["track"]["pieces"]

        for i, item in enumerate(allpieces):
            piece = Piece()
            piece.initpiece(i, item)
            self.pieces.append(piece)
            self.piececount = i
            self.piecelengths.append(piece.length)
            for x in range(int(piece.length)):
                self.speedvalues.append(10)
                self.anglevalues.append(0)

        lastpiece = self.pieces[self.piececount]
        lastpiece.next = 0

    def setname(self, data):
        self.name = data["race"]["track"]["name"]

    def getpiece(self, pieceIndex):
         return self.pieces[pieceIndex]

    def setcornerspeed(self, startpoint, endpoint, speed, acceleration):
        #asetetaan sopivat tavoitespeedit valille... nyt ei ota huomioon viela driftin maaraa, mutta silla voisi asettaa kivat arvot noille speedeille
        brakedistace = self.brakedistance()
        brakespeed = speed


        firsthalfspeed = brakespeed-0.5
        for x in range(int(startpoint), int((endpoint/2))):

            if self.speedvalues[x]>firsthalfspeed:
                self.speedvalues[x] = firsthalfspeed
                #firsthalfspeed = (firsthalfspeed+acceleration)


        for z in range(int((endpoint/2)), int(endpoint)):
            self.speedvalues[z] = (brakespeed*1.1)



    def setangle(self, point, lastpoint, angle):

        for x in range(int(lastpoint), int(point)):
            self.anglevalues[x] = angle

    def targetspeed(self, i):
        index = int(i)
        return self.speedvalues[index] #palauttaa tietyn indeksin speed valuen.

    def getcurrentposition(self, pieceindex, pieceposition):
        piecelengths = 0
        for x in range(1,pieceindex):
            piecelengths = (piecelengths + self.piecelengths[x])
        return (piecelengths + pieceposition)

    def setdriftended(self, position):
        self.driftended = position

    def setdriftstarted(self, position):
        self.driftstarted = position

    def countmaxdrift(self, start, end):
        max =0
        for x in range (int(start), int(end)):
            if self.anglevalues[x]> max:
                max = self.anglevalues[x]
        self.maxdrift = max


    def brakedistance(self):

        angle = self.maxdrift*57
        return int(8*angle)

    def driftspeed(self):
        self.maxspeed =((self.driftstartspeed+self.dirftendspeed)/2)

    def crash(self, position):
        print ("crash korjaus")
        speed = self.speedvalues[int(position)]

        for x in range((int(position-300)), (int(position))):
            self.speedvalues[x] = 3
