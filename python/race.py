__author__ = 'herkko'
from track import Track
from car import Car
import math

class Race(object):

    def __init__(self):
         self.track = Track()
         self.car = Car()
         self.cars = []
         self.piececount = 0
         self.tickcount = 0
         self.accstart = 0
         self.useturbo = 0

    def createrace(self, data):
        self.piececount = self.track.initpieces(data)
        self.initcars(data)


    def initowncar(self, data):
        self.car.name = data['name']
        self.car.color = data['color']

    def initcars(self, data):
        cars = data["race"]["cars"]
        for item in cars:
            car = Car()
            car.initcar(item)
            self.cars.append(car)

    def racestatus(self, data):
        self.carstatus(data)
        if self.car.getcurrentlap() == 0:
            self.updatespeed(data)
        self.countthrottle(data)

        #setup for first learning round
        if self.car.getcurrentlap() == 0:
            self.car.setthrottle(1)
            if self.tickcount == 20:
                self.car.acceleration = (self.car.getspeed())

            if self.tickcount == 21:
                acc = self.car.acceleration
                self.car.acceleration = ((self.car.getspeed()-acc)/10)

            if self.tickcount >1:
                self.car.setthrottle(0.65)


        self.tickcount = self.tickcount +1
        if (self.tickcount%80) ==0:
            print(self.tickcount, self.car.getspeed(), self.track.targetspeed(self.car.getposition()), self.car.drift, )





    def carstatus(self, data):
        for item in data:
            if item['id']['color'] == self.car.color:
                self.car.positionindex = item['piecePosition']['pieceIndex']
                self.car.piecedistance = item['piecePosition']['inPieceDistance']
                if item.has_key("angle"):
                    self.car.drift = math.fabs(item['angle'])
                self.car.approxspeed(item, self.track.getpiece(self.car.positionindex))
                self.car.currentposition = self.track.getcurrentposition(self.car.positionindex, self.car.piecedistance)
                lap = item["piecePosition"]["lap"]
                self.car.setcurrentlap(lap)

    def countthrottle(self, data):

        targetspeed = self.track.targetspeed(self.car.getposition())
        speed =self.car.getspeed()

        if speed < targetspeed:
            self.car.setthrottle(1)
        else:
            self.car.setthrottle(0.01)



    def updatespeed(self,data):
    #katsoo jos pera alkaa luistaa. jos pera luistaa niin korjaa tavoitenopeuksia tuolla valilla pienemmiksi
        carposition = self.car.getposition()
        lastposition = self.car.getlastposition()

        if self.track.driftstarted > 0:
            self.track.setangle(int(carposition), int(lastposition), self.car.drift)
            #Asetetaan angle viimeisimmille paikoille


            if self.car.drift < 0.1:
                self.track.setdriftended(carposition)
                self.track.countmaxdrift(self.track.driftstarted, self.track.driftended)
                self.track.dirftendspeed = self.car.getspeed()
                self.track.driftspeed()

                self.track.setcornerspeed(int(self.track.driftstarted), int(self.track.driftended), self.track.maxspeed, self.car.acceleration)
                self.track.driftstarted = 0
                self.track.driftended = 0

        #drifti alkaa tassa kohtaa.
        else:
            if self.car.drift> 0.1:
                self.track.setdriftstarted(self.car.getposition())
                self.track.driftstartspeed = self.car.getspeed()


    def crash(self, data):
       if (data["color"] == self.car.color):
           self.track.crash(self.car.currentposition)




    def outputspeed(self):
        for x in range (1, len(self.track.speedvalues)):
            if x % 10 == 0:
                print(self.track.speedvalues[x])