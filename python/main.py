import json
import socket
import sys
from car import Car
from track import Track
from race import Race
from helppers import learnPower, createmap


class PetrolHead(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.race = Race()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined-- json test:")
        self.ping()

    def on_init(self, data):
        print("Game initialized")
        self.race.createrace(data)
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.race.racestatus(data)
        self.throttle(self.race.car.throttle)
        if self.race.useturbo >0:
            self.msg("turbo", {})

    def on_crash(self, data):
        print("Someone crashed")
        self.race.crash(data)
        self.ping()

    def on_turbo(self, data):
       print("saatiin turboa")


    def on_game_end(self, data):
        print("Race ended")
        self.race.outputspeed()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_owncar(self, data):
        print("oman auton initalilisointi")
        self.race.initowncar(data)


    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_init,
            'yourCar': self.on_owncar,
            'turboAvailable': self.on_turbo,
            }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = PetrolHead(s, name, key)
        bot.run()




