__author__ = 'herkko'


class Car(object):

    def __init__(self):
        self.name = "default"
        self.color = "default"
        self.carlength = 0
        self.carwidth = 0
        self.power = 1
        self.friction = 1
        self.positionindex = 0 #index of piece
        self.piecedistance = 0 #position in current piece
        self.speed = 0
        self.throttle = 0.65
        self.turbo = False
        self.lastposition = 0
        self.currentposition = 0
        self.drift = 0
        self.currentlap = 0
        self.acceleration = 0
        self.deacceleration = 0

    def initcar(self, data):
        print("car initialization")

        self.name = data['id']['name']
        self.color = data['id']['color']
        self.carlength = data['dimensions']['length']
        self.carwidth = data['dimensions']['width']

    def setthrottle(self, throttle):
        self.throttle = throttle

    def getthrottle(self):
        return self.throttle

    def approxspeed(self, data, piece):
        newposition = data["piecePosition"]["inPieceDistance"]
        length = piece.length
        lastspeed = self.speed

        if self.lastposition < newposition:
            newspeed = (newposition-self.lastposition)
            self.speed = ((lastspeed+newspeed)/2)
        else:
            newspeed = ((length-self.lastposition)+newposition)
            self.speed = ((lastspeed+newspeed)/2)
        self.lastposition = newposition

        if self.speed >15:
            self.speed = 15
        if self.speed<0:
            self.speed = 1

        return self.speed

    def getposition(self):
        return self.currentposition

    def getlastposition(self):
        return self.lastposition

    def getspeed(self):
        return self.speed

    def getcurrentlap(self):
        return self.currentlap

    def setcurrentlap(self, i):
        self.currentlap = i
